package com.zuitt.wdc0043;

import java.util.Scanner;

public class User {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = myObj.nextLine();

        System.out.println("Last Name:");
        String lastName = myObj.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubGrade = myObj.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubGrade = myObj.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubGrade = myObj.nextDouble();

        double average = Math.ceil((firstSubGrade + secondSubGrade + thirdSubGrade) / 3);

        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + average);
    }
}
